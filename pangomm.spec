%define apiver 1.4
%global glibmm_version 2.48.0
%global cairomm_version 1.2.2
%global pango_version 1.45.1

Name:           pangomm
Version:        2.46.4
Release:        1
Summary:        C++ bindings for pango
License:        LGPL-2.1-only and GPL-2.0-or-later
URL:            https://www.gtkmm.org/
Source0:        https://download.gnome.org/sources/pangomm/2.46/%{name}-%{version}.tar.xz

BuildRequires:  meson m4 gcc-c++ doxygen mm-common libxslt
BuildRequires:  pkgconfig(cairomm-1.0) >= %{cairomm_version}
BuildRequires:  glibmm24-devel >= %{glibmm_version}
BuildRequires:  pango-devel >= %{pango_version}
BuildRequires:  cmake

Requires:       glibmm24%{?_isa} >= %{glibmm_version}
Requires:       cairomm%{?_isa} >= %{cairomm_version}
Requires:       pango%{?_isa} >= %{pango_version}

%description
Pangomm provides a C++ interface to the Pango library.

%package devel
Summary:        Development package for %{name}
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains some header and library files for the development
of %{name}.

%package          help
Summary:          Help package for %{name}
BuildArch:        noarch
Requires:         %{name} = %{version}-%{release}
Requires:         libsigc++20-doc glibmm24-doc
Provides:         %{name}-doc = %{version}-%{release}
Obsoletes:        %{name}-doc < %{version}-%{release}

%description      help
This package contains some man help file and some other files for %{name}.

%prep
%autosetup -p1
doxygen -u docs/reference/Doxyfile.in
%build
%meson -Dbuild-documentation=true
%meson_build

%install
%meson_install
%delete_la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license COPYING
%doc NEWS
%{_libdir}/libpangomm-%{apiver}.so.1*

%files devel
%{_includedir}/pangomm-%{apiver}
%{_libdir}/libpangomm-%{apiver}.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/pangomm-%{apiver}

%files help
%doc %{_docdir}/pangomm-%{apiver}/
%{_datadir}/devhelp/

%changelog
* Tue Mar 19 2024 xu_ping <707078654@qq.com> - 2.46.4-1
- Update to 2.46.4

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 2.46.3-1
- Update to 2.46.3

* Mon Jun 20 2022 liukuo <liukuo@kylinos.cn> - 2.46.2-2
- License compliance rectification

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.46.2-1
- Update to 2.46.2

* Fri Jun 18 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 2.42.2-1
- Upgrade to 2.42.2
- Use meson rebuild

* Wed Dec 11 2019 catastrowings <jianghuhao1994@163.com> - 2.40.1-7
- openEuler init
